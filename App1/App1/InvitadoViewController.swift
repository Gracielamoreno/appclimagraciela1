import UIKit
import CoreLocation

class InvitadoViewController: UIViewController, CLLocationManagerDelegate{
    
    //MARK:- Outlets
      let locationManager = CLLocationManager()
      var didGetWheather = false
  
   
    
  
    @IBOutlet weak var Label: UILabel!
    
    @IBOutlet weak var Label1: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
 
        locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse {
            locationManager.delegate = self
            locationManager.startUpdatingLocation()
        }
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = manager.location?.coordinate
        if !didGetWheather {
            consultarButtonPressed(
                lat: (location?.latitude)!,
                lon: (location?.longitude)!
            )
            didGetWheather = true
        }
    }
       
    
    

    private func consultarButtonPressed(lat:Double,lon:Double) {
        let service = servicio()
        service.consultarporUbicacion(lat:lat, lon:lon ){(weather)in
            DispatchQueue.main.async {
                self.Label.text = weather
            }
            }
        
       
    }
    
    
}
    

    



//
//  servicio.swift
//  App1
//
//  Created by Graciela Moreno on 7/11/17.
//  Copyright © 2017 kevin. All rights reserved.
//

import Foundation
class servicio{
    func consultarporCiudad(city:String,completion:@escaping (String)-> ()){
    
    //http://samples.openweathermap.org/data/2.5/weather?q=London,uk&appid=76f9031b8d973f7baa9eeebe8f0ca5e7
    
    
    let urlStr = "http://api.openweathermap.org/data/2.5/weather?q=\(city);uk&appid=f41020d6f457c1bf6a9ec1556b596a7b"
    
        consultar(urlStr: urlStr){(weather) in completion(weather)}
    
}
    func consultarporUbicacion(lat:Double,lon:Double,completion:@escaping (String)-> ()){
        
        //http://samples.openweathermap.org/data/2.5/weather?q=London,uk&appid=76f9031b8d973f7baa9eeebe8f0ca5e7
        
        
        let urlStr = "http://api.openweathermap.org/data/2.5/weather?lat=\(lat)&lon=\(lon)&appid=7d95e6fefc2b7875252004fead0caaeb"
        
        consultar(urlStr:urlStr){(weather) in completion(weather)}
    
}
    
private func consultar(urlStr:String,completion:@escaping (String)-> ()) {
    let url = URL(string: urlStr)
    let request = URLRequest(url: url!)
    let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
        if let _ = error {
            return
        }
        let weatherdata = data as! Data
        do {
            let weatherJson = try JSONSerialization.jsonObject(with: weatherdata, options: [])
            let weatherDictionary = weatherJson as! NSDictionary
            let weatherArray = weatherDictionary["weather"] as! NSArray
            let weather = weatherArray[0] as! NSDictionary
            let weatherDesc = weather["description"] ?? "error"
            DispatchQueue.main.async {
                //self.Label.text = "\(weatherDesc)"
            }
        }
        catch{
            print("Error al generar el Json")
            
        }
    }
    task.resume()
}

}
